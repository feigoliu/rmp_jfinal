package com.dcits.business.server.linux;

public class LinuxExtraParameter {
	
	protected String javaHome;

	public LinuxExtraParameter(String javaHome) {
		super();
		this.javaHome = javaHome;
	}

	public LinuxExtraParameter() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getJavaHome() {
		return javaHome;
	}

	public void setJavaHome(String javaHome) {
		this.javaHome = javaHome;
	}
	
	
	
}
