package com.dcits.business.server.tomcat;

import com.dcits.business.server.MonitoringInfo;

public class TomcatMonitoringInfo extends MonitoringInfo {
	
	/**
	 * 启动时间
	 */
	private String startTime;
	/**
	 * 连续工作时间
	 */
	private String upTime;
	/**
	 * 运行模式：bio、nio、apr<br>
	 * 性能依次增强
	 */
	private String runType;
	
	/**
	 * 最大会话数
	 */
	private String sessionMaxActiveCount;
	/**
	 * 会话数
	 */
	private String sessionActiveCount;
	/**
	 * 活动会话数
	 */
	private String sessionCounter;
	
	/**
	 * 最大线程数
	 */
	private String threadMaxCount;
	/**
	 * 当前线程数
	 */
	private String threadCurrentCount;
	/**
	 * 当前繁忙线程数
	 */
	private String threadCurrentBusyCount;
	
	private Integer webPort;
	
	private String projectPath;
	
	public Integer getWebPort() {
		return webPort;
	}
	public void setWebPort(Integer webPort) {
		this.webPort = webPort;
	}
	public String getProjectPath() {
		return projectPath;
	}
	public void setProjectPath(String projectPath) {
		this.projectPath = projectPath;
	}
	public String getRunType() {
		return runType;
	}
	public void setRunType(String runType) {
		this.runType = runType;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getUpTime() {
		return upTime;
	}
	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}
	public String getSessionMaxActiveCount() {
		return sessionMaxActiveCount;
	}
	public void setSessionMaxActiveCount(String sessionMaxActiveCount) {
		this.sessionMaxActiveCount = sessionMaxActiveCount;
	}
	public String getSessionActiveCount() {
		return sessionActiveCount;
	}
	public void setSessionActiveCount(String sessionActiveCount) {
		this.sessionActiveCount = sessionActiveCount;
	}
	public String getSessionCounter() {
		return sessionCounter;
	}
	public void setSessionCounter(String sessionCounter) {
		this.sessionCounter = sessionCounter;
	}
	public String getThreadMaxCount() {
		return threadMaxCount;
	}
	public void setThreadMaxCount(String threadMaxCount) {
		this.threadMaxCount = threadMaxCount;
	}
	public String getThreadCurrentCount() {
		return threadCurrentCount;
	}
	public void setThreadCurrentCount(String threadCurrentCount) {
		this.threadCurrentCount = threadCurrentCount;
	}
	public String getThreadCurrentBusyCount() {
		return threadCurrentBusyCount;
	}
	public void setThreadCurrentBusyCount(String threadCurrentBusyCount) {
		this.threadCurrentBusyCount = threadCurrentBusyCount;
	}
}
