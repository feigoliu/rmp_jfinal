package com.dcits.config;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.dcits.business.server.ServerType;
import com.dcits.business.userconfig.UserSpace;
import com.dcits.constant.ConstantGlobalAttributeName;
import com.dcits.constant.ConstantInit;
import com.dcits.interceptor.GlobalInterceptor;
import com.dcits.mvc.common.controller.FileInfoController;
import com.dcits.mvc.common.controller.ServerInfoController;
import com.dcits.mvc.common.controller.UserConfigController;
import com.dcits.mvc.common.model._MappingKit;
import com.dcits.tool.ThreadPoolUtil;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;

public class JFinalConfig extends com.jfinal.config.JFinalConfig {

	
	private static final Logger logger = Logger.getLogger(JFinalConfig.class);
	/**
	 * 配置常量
	 */
	@Override
	public void configConstant(Constants me) {
		// TODO Auto-generated method stub
		logger.info("加载init.properties配置文件...");
		PropKit.use("init.properties");
		logger.info("Dev_Mode=" + PropKit.getBoolean(ConstantInit.config_devMode, false));
		me.setDevMode(PropKit.getBoolean(ConstantInit.config_devMode, false));
	}

	public static DruidPlugin createDruidPlugin() {
		return new DruidPlugin(PropKit.get(ConstantInit.db_connection_jdbc_url), PropKit.get(ConstantInit.db_connection_username), 
				PropKit.get(ConstantInit.db_connection_password).trim(), PropKit.get(ConstantInit.db_connection_driver_class));
	}
	
	/**
	 * 配置路由
	 */
	@Override
	public void configRoute(Routes me) {
		// TODO Auto-generated method stub
		me.add("/server", ServerInfoController.class);
		me.add("/config", UserConfigController.class);
		me.add("/report", FileInfoController.class);
		logger.info("加载server.properties配置文件...");
		ServerType.setAllServerType();
		
		for (String typeName:ServerType.controllerClasses.keySet()) {
			me.add("/" + typeName, (Class<Controller>) ServerType.controllerClasses.get(typeName));
		}
		
	}
	
	@Override
	public void afterJFinalStart() {
		// TODO Auto-generated method stub
		JFinal.me().getServletContext().setAttribute(ConstantGlobalAttributeName.USER_SPACE_ATTRITURE_NAME,
				new HashMap<String, UserSpace>());

	}
	
	
	@Override
	public void beforeJFinalStop() {
		// TODO Auto-generated method stub
		ThreadPoolUtil.shutdownPool();
	}

	@Override
	public void configEngine(Engine me) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * 配置插件
	 */
	@Override
	public void configPlugin(Plugins me) {
		// TODO Auto-generated method stub
		logger.info("加载Druid数据连接池插件...");
		//配置数据连接池
		DruidPlugin druidPlugin = createDruidPlugin();
		druidPlugin.set(PropKit.getInt(ConstantInit.db_initialSize), PropKit.getInt(ConstantInit.db_minIdle),
				PropKit.getInt(ConstantInit.db_maxActive));
		me.add(druidPlugin);
		logger.info("加载ActiveRecordPlugin插件...");
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		//arp.setShowSql(true);
		
		_MappingKit.mapping(arp);
		me.add(arp);
	}

	@Override
	public void configInterceptor(Interceptors me) {
		// TODO Auto-generated method stub
		me.addGlobalActionInterceptor(new GlobalInterceptor());
	}

	@Override
	public void configHandler(Handlers me) {
		// TODO Auto-generated method stub
		
	}

}
